﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InputLibrary;
using MathLibrary;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            double a, b;
            char action;

            a = ConsoleInput.InputDouble("input a: ");
            action = ConsoleInput.InputChar("input action(+ - * /): ");
            b = ConsoleInput.InputDouble("input b: ");

            switch (action)
            {
                case '+':
                    Console.WriteLine($"Sum = {MyMath.Sum(a,b)}");
                    break;
                case '-':
                    Console.WriteLine($"Sub = {MyMath.Sub(a, b)}");
                    break;
                case '*':
                    Console.WriteLine($"Sub = {MyMath.Mul(a, b)}");
                    break;
                case '/':
                    if (b == 0)
                    {
                        Console.WriteLine("Error / by 0");
                    }
                    else
                    {
                        Console.WriteLine($"Sub = {MyMath.Dev(a, b)}");
                    }
                    break;
            }

            Console.ReadKey();
        }
    }
}
