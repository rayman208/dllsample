﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InputLibrary
{
    public class ConsoleInput
    {
        public static double InputDouble(string msg)
        {
            double x;
            bool inputResult;

            do
            {
                Console.Write(msg);
                inputResult = double.TryParse(Console.ReadLine(), out x);
            }
            while (inputResult==false);

            return x;
        }

        public static char InputChar(string msg)
        {
            char x;
            bool inputResult;

            do
            {
                Console.Write(msg);
                inputResult = char.TryParse(Console.ReadLine(), out x);
            }
            while (inputResult == false);

            return x;
        }
    }
}
